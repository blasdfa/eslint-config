# @air/eslint-config

## Использование

### Установка

```bash
pnpm i -D eslint @air/eslint-config
```

И создайте `eslint.config.mjs` в корневом каталоге вашего проекта:

```js
// eslint.config.mjs
import air from '@air/eslint-config'

export default air()
```

<details>
<summary>
Использование с устаревшей конфигурацией:
</summary>

Если вы все еще используете конфигурации из устаревшего формата `.eslintrc`, вы можете использовать пакет [`@eslint/eslintrc`](https://www.npmjs.com/package/@eslint/eslintrc), чтобы преобразовать их в стандартную конфигурацию.

```js
// eslint.config.mjs
import air from '@air/eslint-config'
import { FlatCompat } from '@eslint/eslintrc'

const compat = new FlatCompat()

export default air(
  {
    ignores: [],
  },

  // Старый конфиг
  ...compat.config({
    extends: [
      'eslint:recommended',
      // ...
    ],
  })

  // ...
)
```

</details>

### Добавить скрипт в package.json

```json
{
  "scripts": {
    "lint": "eslint .",
    "lint:fix": "eslint . --fix"
  }
}
```

## Поддержка VS Code (Автоисправление при сохранении)

Установите [VS Code ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

Добавьте следующие настройки в свой файл `.vscode/settings.json`:

```jsonc
{
  // Enable the ESlint flat config support
  "eslint.experimental.useFlatConfig": true,

  // Disable the default formatter, use eslint instead
  "prettier.enable": false,
  "editor.formatOnSave": false,

  // Auto fix
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": "explicit",
    "source.organizeImports": "never"
  },

  // Silent the stylistic rules in you IDE, but still auto fix them
  "eslint.rules.customizations": [
    { "rule": "style/*", "severity": "off" },
    { "rule": "format/*", "severity": "off" },
    { "rule": "*-indent", "severity": "off" },
    { "rule": "*-spacing", "severity": "off" },
    { "rule": "*-spaces", "severity": "off" },
    { "rule": "*-order", "severity": "off" },
    { "rule": "*-dangle", "severity": "off" },
    { "rule": "*-newline", "severity": "off" },
    { "rule": "*quotes", "severity": "off" },
    { "rule": "*semi", "severity": "off" }
  ],

  // Enable eslint for all supported languages
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "typescript",
    "typescriptreact",
    "vue",
    "html",
    "markdown",
    "json",
    "jsonc",
    "yaml",
    "toml",
    "gql",
    "graphql",
    "astro"
  ]
}
```
